# DroneZone ~ Front-end

Front-end for a drone and high tech online hobby and sports toy store, built using React.js, React Router, and Tailwind CSS. Enjoy!

Live **Site Demo** ~ [DroneZone - front-end](http://financialreact.ryanhunter.ca/)

![DroneZone - front-end](http://ryanhunter.ca/images/portfolio/financialreact01.png)

## Features

- React.js
- React Router 5
- Tailwind CSS framework
- FontAwesome 5 icons
- Google Fonts
- Filler text from https://pirateipsum.me  

## License

This project is licensed under the terms of the **MIT** license.

## Screenshots

![DroneZone - front-end](http://ryanhunter.ca/images/portfolio/financialreact01.png)

![DroneZone - front-end](http://ryanhunter.ca/images/portfolio/financialreact02.png)
