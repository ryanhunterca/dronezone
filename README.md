# dronezone

Full-stack web app for an online flying drone and high tech hobby store.  Front-end built with React.js, React Router, and Tailwind CSS.  Backend provided by a JSON datastore, Note.js, and Express.js.  Uses Stripe.com for payment processing.

## System Requirements

- Node 10+
- NPM 5+
